# GPS Logger

Welcome to my GPS Logger project.  This is a side project I have been working on just for fun/practice.

## This project will consist of three components:

### NMEA 0183 Data Parser

This library will support parsing NMEA 0183 sentences and generating .NET events

Parses the standard NMEA 0183 data format, used by most GPS receivers.  I use the GlobalSat BU-353-S4 USB GPS Receiver: http://a.co/d/6AzYbf9

There are many websites that have information about this format.  I mainly have been using this site for decoding the structure: https://www.gpsinformation.org/dale/nmea.htm.

There is a very useful website to generate data here: http://freenmea.net/emitter

### GPS Logging client

A NMEA parser client that will persist data to a database.

### Main Application

A command line application that will connect to a specified serial port with the GPS attached and will connect listener clients.

### Future Additions

More clients are planned.  Would be nice to create a simple thick or thin client for data visualization on a map.
