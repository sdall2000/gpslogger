﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DallTech.Nmea0183;

namespace DallTech.Nmea0183Tests
{
    [TestClass]
    public class Nmea0183ParserTest
    {
        [TestMethod]
        public void ParserTest()
        {
            string nmeaSentence = "$GPGGA,015856.306,3654.928,N,07502.498,W,0,00,,,M,,M,,*59\n\r";

            Parser parser = new Parser();

            foreach(char c in nmeaSentence)
            {
                parser.PushByte((byte) c);
            }
        }
    }
}
