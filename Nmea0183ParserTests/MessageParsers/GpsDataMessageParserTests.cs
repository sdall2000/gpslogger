﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DallTech.Nmea0183.Messages;
using Nmea0183Parser.MessageParsers;

namespace DallTech.Nmea0183.MessageParsers.Tests
{
    [TestClass()]
    public class GpsDataMessageParserTests
    {
        [TestMethod()]
        public void parseTest()
        {
            string nmeaSentence = "$GPGGA,015856.306,3654.928,N,07502.498,W,0,00,,,M,,M,,*59\n\r";
            GpsDataMessage gpsData = GpsDataMessageParser.Parse(nmeaSentence);
            Assert.AreEqual(CommonParser.GpsDataIdentifier, gpsData.SentenceIdentifier);
        }
    }
}