﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Nmea0183Parser.MessageParsers.Tests
{
    [TestClass()]
    public class CommonParserTests
    {
        [TestMethod()]
        public void ParseSentenceIdentifierTest()
        {
            string nmeaSentence = "$GPGGA,015856.306,3654.928,N,07502.498,W,0,00,,,M,,M,,*59\n\r";

            // Should be able to parse out the identifier even if the whole sentence is supplied.
            Assert.AreEqual("GPGGA", CommonParser.ParseSentenceIdentifier(nmeaSentence));

            // Verify parsing when just the identifier field is supplied.
            Assert.AreEqual("GPGGA", CommonParser.ParseSentenceIdentifier("$GPGGA"));
        }

        [TestMethod()]
        public void ParseUtcTimeTest()
        {
            // Format is HHMMSS relative to UTC.
            TimeSpan ts = CommonParser.ParseUtcTime("015856");

            Assert.AreEqual(new TimeSpan(1, 58, 56), ts);

            // TODO add unit test for fractional seconds once that is supported.
        }

        [TestMethod()]
        public void ParseLatitudeTest()
        {
            // TODO make the delta more precise based on the resolution of the NMEA field.
            Assert.AreEqual(36.915467, CommonParser.ParseLatitude("3654.928", "N"), 0.000001);
            Assert.AreEqual(-36.915467, CommonParser.ParseLatitude("3654.928", "S"), 0.000001);
        }

        [TestMethod()]
        public void ParseLongitudeTest()
        {
            Assert.AreEqual(75.041633, CommonParser.ParseLongitude("07502.498", "E"), 0.000001);
            Assert.AreEqual(-75.041633, CommonParser.ParseLongitude("07502.498", "W"), 0.000001);
        }

        [TestMethod()]
        public void ParseByteTest()
        {
            Assert.AreEqual(5, CommonParser.ParseByte("5"));
            Assert.AreEqual(255, CommonParser.ParseByte("255"));
        }

        [TestMethod()]
        public void ParseDoubleTest()
        {
            Assert.AreEqual(1.2345, (double) CommonParser.ParseDouble("1.2345"), 0.1);
        }

        [TestMethod()]
        public void ParseAltitudeMetersTest()
        {
            // TODO more tests, what other units are supported?
            Assert.AreEqual(123, CommonParser.ParseAltitudeMeters("123", "M"));

            Assert.IsNull(CommonParser.ParseAltitudeMeters("", ""));
        }
    }
}