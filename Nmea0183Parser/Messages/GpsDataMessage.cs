﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DallTech.Nmea0183.Messages
{
    public class GpsDataMessage
    {
        public string SentenceIdentifier { get; set; }
        public TimeSpan UtcTime { get; set; }
        public double LatitudeDegrees { get; set; }
        public double LongitudeDegrees { get; set; }
        public byte FixQuality { get; set; }
        public byte NumberOfSatellites { get; set; }
        public double? Hdop { get; set; }
        
        // TODO should we split up the value and the units?
        public double? AltitudeMslMeters { get; set; }
        public double? HeightAboveEllipsoidMeters { get; set; }

        public TimeSpan TimeSinceLastDgpsUpdate { get; set; }
        public string DgpsStationId { get; set; }
        public byte Checksum { get; set; }
    }
}
