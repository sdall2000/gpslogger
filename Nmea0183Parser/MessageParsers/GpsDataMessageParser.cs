﻿using DallTech.Nmea0183.Messages;
using Nmea0183Parser.MessageParsers;
using System;

namespace DallTech.Nmea0183.MessageParsers
{
    public class GpsDataMessageParser
    {
        public static GpsDataMessage Parse(String sentence)
        {
            GpsDataMessage gpsData = new GpsDataMessage();

            // Really we already know this is a GPGGA message.  And, this field is common
            // to all messages.  Probably should be in a superclass.
            gpsData.SentenceIdentifier = CommonParser.ParseSentenceIdentifier(sentence);

            // Split the sentence at commas.
            string[] parts = sentence.Split(',');

            gpsData.UtcTime = CommonParser.ParseUtcTime(parts[1]);
            gpsData.LatitudeDegrees = CommonParser.ParseLatitude(parts[2], parts[3]);
            gpsData.LongitudeDegrees = CommonParser.ParseLongitude(parts[4], parts[5]);
            gpsData.FixQuality = CommonParser.ParseByte(parts[6]);
            gpsData.NumberOfSatellites = CommonParser.ParseByte(parts[7]);
            gpsData.Hdop = CommonParser.ParseDouble(parts[8]);
            gpsData.AltitudeMslMeters = CommonParser.ParseAltitudeMeters(parts[9], parts[10]);
            gpsData.HeightAboveEllipsoidMeters = CommonParser.ParseAltitudeMeters(parts[11], parts[12]);

            // TODO
            // gpsData.TimeSinceLastDgpsUpdate
            // gpsData.DgpsStationId
            // gpsData.Checksum

            return gpsData;
        }
    }
}
