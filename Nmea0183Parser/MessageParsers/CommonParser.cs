﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nmea0183Parser.MessageParsers
{
    public class CommonParser
    {
        public const String GpsDataIdentifier = "GPGGA";

        public static string ParseSentenceIdentifier(String sentence)
        {
            return sentence.Substring(1, 5);
        }

        public static TimeSpan ParseUtcTime(String utcTime)
        {
            // Format is hh:mm:ss
            int h = System.Convert.ToInt32(utcTime.Substring(0, 2));
            int m = System.Convert.ToInt32(utcTime.Substring(2, 2));
            int s = System.Convert.ToInt32(utcTime.Substring(4, 2));

            // TODO the sample data we downloaded has fractions of a second.

            return new TimeSpan(h, m, s);
        }

        public static double ParseLatitude(String degrees, String hemisphere)
        {
            // Typical lat/hemi combo might look like this: 4807.038,N
            // Which translates to, 48 degrees, 7.038 minutes, north.
            // This will be converted to decimal degrees.
            int degreesPart = Convert.ToInt32(degrees.Substring(0, 2));
            double minutesPart = Convert.ToDouble(degrees.Substring(2)) / 60.0;
            int sign = hemisphere.Equals("N", StringComparison.OrdinalIgnoreCase) ? 1 : -1;

            return sign * (degreesPart + minutesPart);
        }

        public static double ParseLongitude(String degrees, String hemisphere)
        {
            // Typical lon/hemi combo might look like this: 01131.038,E
            // Which translates to, 11 degrees, 31.038 minutes, east.
            // This will be converted to decimal degrees.
            int degreesPart = Convert.ToInt32(degrees.Substring(0, 3));
            double minutesPart = Convert.ToDouble(degrees.Substring(3)) / 60.0;
            int sign = hemisphere.Equals("E", StringComparison.OrdinalIgnoreCase) ? 1 : -1;

            return sign * (degreesPart + minutesPart);
        }

        public static byte ParseByte(String byteString)
        {
            return Convert.ToByte(byteString);
        }

        public static double? ParseDouble(String doubleString)
        {
            double? dbl = null;

            if (doubleString.Length > 0)
            {
                dbl = Convert.ToDouble(doubleString);
            }

            return dbl;
        }

        public static double? ParseAltitudeMeters(String altitudeString, String units)
        {
            // TODO currently assuming meters.  Need to implement other possible units.
            return ParseDouble(altitudeString);
        }
    }
}
