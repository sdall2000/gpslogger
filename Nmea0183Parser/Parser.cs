﻿using System;
using Nmea0183Parser.MessageParsers;
using DallTech.Nmea0183.Messages;
using DallTech.Nmea0183.MessageParsers;

namespace DallTech.Nmea0183
{
    enum State { HeaderSeek, EolSeek };

    public class Parser
    {
        public event EventHandler<GpsDataMessage> RaiseGpsDataEvent;

        // Sample NMEA sentence.
        // $GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*42
        int TimeoutMilliseconds;
        long LastDataMilliseconds = 0;
        State state = State.HeaderSeek;
        String sentence = "";

        public Parser() : this(0)
        {
        }

        public Parser(int timeoutMilliseconds)
        {
            TimeoutMilliseconds = timeoutMilliseconds;
        }

        public void PushByte(byte b)
        {
            // See if data has timed out.
            if (TimeoutMilliseconds > 0 && LastDataMilliseconds != 0)
            {
                long milliseconds = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

                if (milliseconds - LastDataMilliseconds >= TimeoutMilliseconds)
                {
                    // Data timed out.  Reset and seek header.
                    // TODO throw some kind of event indicating timeout.
                    state = State.HeaderSeek;
                    sentence = "";
                }
                LastDataMilliseconds = milliseconds;
            }

            switch (state)
            {
                case State.HeaderSeek:
                    if (b == '$')
                    {
                        sentence += (char)b;
                        state = State.EolSeek;
                    }
                    break;
                case State.EolSeek:
                    if (b == '\n' || b == '\r')
                    {
                        // Process sentence on CR or LF.
                        // TODO Detect checksum field then process, rather than waiting for CR/LF.
                        ProcessSentence(sentence);

                        state = State.HeaderSeek;
                        sentence = "";
                    }
                    else
                    {
                        // Just append character to data stream.
                        sentence += (char)b;
                    }
                    break;
            }
        }

        private void ProcessSentence(String sentence)
        {
            // We have the expected header character, not extract the sentence identifier.
            string sentenceIdentifier = CommonParser.ParseSentenceIdentifier(sentence);

            switch (sentenceIdentifier)
            {
                case CommonParser.GpsDataIdentifier:
                    GpsDataMessage message = GpsDataMessageParser.Parse(sentence);
                    EventHandler<GpsDataMessage> handler = RaiseGpsDataEvent;

                    // Handler will be null if there are no subscribers
                    handler?.Invoke(this, message);

                    break;
            }
        }
    }
}
