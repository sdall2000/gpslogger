﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DallTech.Nmea0183.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Parser parser = new Parser();

            parser.RaiseGpsDataEvent += Parser_RaiseGpsDataEvent;

            Options options = ParseCommandLine(args);

            if (options.Filename != null)
            {
                Console.WriteLine("Parsing file " + options.Filename);
                Console.WriteLine("Current dir: " + Directory.GetCurrentDirectory());

                byte[] bytes = File.ReadAllBytes(options.Filename);

                foreach (byte b in bytes)
                {
                    parser.PushByte(b);
                }
            }
            else
            {
                Console.WriteLine("Nothing to do - please specify a file to parse");
                Console.WriteLine("Current dir: " + Directory.GetCurrentDirectory());
            }
        }

        private static Options ParseCommandLine(string[] args)
        {
            Options options = new Options();

            // For now, assume the first parameter is the filename.
            if (args.Length == 1)
            {
                options.Filename = args[0];
            }

            return options;
        }

        private static void Parser_RaiseGpsDataEvent(object sender, Messages.GpsDataMessage e)
        {
            Console.WriteLine("Received a gps data event: " + e.LatitudeDegrees + "/" + e.LongitudeDegrees);
        }
    }

    class Options
    {
        public String Filename { get; set; }
    }
}
